import {
  defineConfig
} from 'vite';
import vue from '@vitejs/plugin-vue';
import {
  resolve
} from 'path';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      // Assurez-vous que le chemin pointe vers le répertoire 'src' de votre projet
      '@': resolve(__dirname, './src')
    }
  }
});